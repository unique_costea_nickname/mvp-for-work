# My personal file configurations nothing more than this

## Apps List
* [Cloud App](https://www.getcloudapp.com/) for screen recording and screenshots
* [Flux](https://justgetflux.com/) for color safe
* [iTerm](https://www.iterm2.com/) just another terminal
* [React Native Debugger App](https://github.com/jhen0409/react-native-debugger)
* [Sublime Text](https://www.sublimetext.com/)
* Telegram __from the store__
* [Visual Studio Code](https://code.visualstudio.com/)
* [yarn](https://yarnpkg.com/lang/en/docs/install/)


## VSCODE extensions:
* [https://github.com/dzannotti/vscode-babel/](https://github.com/dzannotti/vscode-babel/)
* [https://github.com/CoenraadS/BracketPair](https://github.com/CoenraadS/BracketPair)
* [https://github.com/Microsoft/vscode-eslint](https://github.com/Microsoft/vscode-eslint)
* [https://github.com/eamodio/vscode-gitlens](https://github.com/eamodio/vscode-gitlens)
* [https://github.com/xabikos/vscode-javascript](https://github.com/xabikos/vscode-javascript)
* [https://github.com/shd101wyy/vscode-markdown-preview-enhanced](https://github.com/shd101wyy/vscode-markdown-preview-enhanced)
* [https://github.com/discountry/vscode-react-redux-react-router-snippets](https://github.com/discountry/vscode-react-redux-react-router-snippets)
* [https://github.com/Tyriar/vscode-sort-lines](https://github.com/Tyriar/vscode-sort-lines)
* [https://github.com/shinnn/vscode-stylelint](https://github.com/shinnn/vscode-stylelint)
* [https://github.com/Microsoft/vscode-sublime-keybindings](https://github.com/Microsoft/vscode-sublime-keybindings)
* [https://github.com/vscode-icons/vscode-icons](https://github.com/vscode-icons/vscode-icons)
* [https://github.com/iFwu/vscode-styled-jsx](https://github.com/iFwu/vscode-styled-jsx)
